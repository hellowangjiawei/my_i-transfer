#ifndef __YOLOV3_USART_H__
#define __YOLOV3_USART_H__
#include "delay.h"
#include "sys.h"

extern unsigned char firstNum;//接收的第一个数字
extern unsigned char numGet;//最终得到的数字
extern unsigned char Isnumget_OK;//接收完成标志位

void yolov3_uart_init(u32 bound);
void USART1_IRQHandler(void);



#endif

