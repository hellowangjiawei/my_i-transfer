#include "stm32f10x.h"                  // Device header
/**
 * @brief LED初始化
 * @param 无
 * @retval 无
 */

void LED_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	GPIO_InitTypeDef GPIO_Initstructure;
	GPIO_Initstructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Initstructure.GPIO_Pin=GPIO_Pin_3|GPIO_Pin_2;
	GPIO_Initstructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOC,&GPIO_Initstructure);
	GPIO_SetBits(GPIOC,GPIO_Pin_3|GPIO_Pin_2);
}

/**
 * @brief LED状态控制
 * @param 1 or 0（1为亮，2为灭）
 * @retval 无
 */

void LED1(unsigned char a)
{
	a == 1 ? GPIO_ResetBits(GPIOC, GPIO_Pin_3) : GPIO_SetBits(GPIOC, GPIO_Pin_3);
}

void LED2(unsigned char a)
{
	a == 1 ? GPIO_ResetBits(GPIOC, GPIO_Pin_2) : GPIO_SetBits(GPIOC, GPIO_Pin_2);
}