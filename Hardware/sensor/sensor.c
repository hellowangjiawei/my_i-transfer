#include "sensor.h"


void sensor_init(void)
{
 	GPIO_InitTypeDef GPIO_InitStructure;
 
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);//使能PORTA,PORTE时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //设置成浮空输入
 	GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化
}

/**
 * @brief 读取红外值
 * @param 无
 * @retval 红外返回值
 */

int read_sensor(void)
{
	return GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_5);
}


